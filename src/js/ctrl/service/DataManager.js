/*
 * This file is part of Pic4Carto.
 *
 * Pic4Carto is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * Pic4Carto is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Pic4Carto.  If not, see <http://www.gnu.org/licenses/>.
 */

const L = require("leaflet");
const Cell = require("../../model/Cell");
const CellStore = require("./CellStore");
const P4C = require("pic4carto");
const EventLauncher = P4C.EventLauncher;
const PicturesManager = P4C.PicturesManager;
const GridFactory = require("../../model/GridFactory");
const UDF = require("../UserDefinedFetchers");

const MAPILLARY_CLIENT_ID = "MLY|4530368186982514|b1bade974dc891184edffd82453b219e";
const FLICKR_API_KEY = "b8c50865a2c22d93d3e673de8bd50178";

/**
 * DataManager retrieves pictures metadata from CellStore or directly from pictures providers.
 */
class DataManager extends EventLauncher {
//CONSTRUCTOR
	constructor() {
		super();
		this.cellStore = new CellStore();

		//Fetchers definition
		this.picManOpts = {
			fetcherCredentials: {
				flickr: { key: FLICKR_API_KEY },
				mapillary: { key: MAPILLARY_CLIENT_ID }
			},
			userDefinedFetchers: UDF
		};

		this.picMan = new PicturesManager(this.picManOpts);

		//Fetcher events
		this.picMan.on("fetcherdone", fid => {
			this.fire("fetcherdone", fid);
		});

		this.picMan.on("fetcherfailed", fid => {
			this.fire("fetcherfailed", fid);
		});
	}

//OTHER METHODS
	/**
	 * Ask for pictures metadata retrieval.
	 * You might suscribe to fetcherdone or fetcherfailed events to watch download progress.
	 * @param {LatLngBounds} bbox The area in which data must be retrieved
	 * @param {Object} [options] The options
	 * @param {long} [options.start] The start timestamp, in milliseconds since 1st january 1970.
	 * @param {long} [options.end] The end timestamp, in milliseconds since 1st january 1970.
	 * @param fresh Force download from the web (optional, false if undefined)
	 * @return {Promise} A promise resolving on pictures metadata
	 */
	ask(bbox, options, fresh) {
		return new Promise((resolve, reject) => {
			fresh = fresh || false;
			options = options || {};

			if(options.start) { options.start = this.roundDate(options.start); }
			else { options.start = this.roundDate(Date.now() - 6 * 30 * 24 * 60 * 60 * 1000); }

			if(options.end) { options.end = this.roundDate(options.end) + 24*60*60*1000; }
			else { options.end = null; }

			const result = GridFactory.make(bbox, options);
			const cachedCellIds = [];

			/*
			* Retrieve cached data
			*/
			let fetchersBbox = bbox;

			if(!fresh) {
				fetchersBbox = null;

				for(let i=0; i < result.length; i++) {
					let cachedCell = this.cellStore.get(result[i].getId());

					//If cell exists and recent enough, store it in result
					if(cachedCell !== null && (!isNaN(options.end) || cachedCell.creationDate >= Date.now() - 6 * 60 * 60 * 1000)) {
						result[i] = cachedCell;
						cachedCellIds.push(i);
					}
					//Add cell area in fetchers bbox
					else {
						if(fetchersBbox == null) {
							fetchersBbox = result[i].bounds;
						}
						else {
							fetchersBbox.extend(result[i].bounds);
						}
					}
				}
			}
			else {
				this.cellStore.clear();
			}

			//Convert fetchersBbox to P4C format
			if(fetchersBbox !== null) {
				fetchersBbox = new P4C.LatLngBounds(new P4C.LatLng(fetchersBbox.getSouth(), fetchersBbox.getWest()), new P4C.LatLng(fetchersBbox.getNorth(), fetchersBbox.getEast()));
			}

			/*
			* Retrieve data from fetchers
			*/
			if(cachedCellIds.length < result.length) {
				this.picMan.startPicsRetrieval(fetchersBbox, { mindate: options.start, maxdate: options.end, usefetchers: options.fetchers })
				.then(pictures => {
					//Update cells
					for(let i=0; i < pictures.length; i++) {
						pictures[i].coordinates = L.latLng(pictures[i].coordinates.lat, pictures[i].coordinates.lng);
						let coords = pictures[i].coordinates;
						let foundCell = null;
						let cellId = 0;

						//Search the cell containing the current picture
						while(foundCell == null && cellId < result.length) {
							if(result[cellId].bounds.contains(coords)) {
								foundCell = result[cellId];
							}
							else {
								cellId++;
							}
						}

						if(foundCell) {
							//If the pictures is on a cached cell, we renew completely the cell
							if(cachedCellIds.indexOf(cellId) >= 0) {
								result[cellId] = new Cell(foundCell.bounds, foundCell.options);
								foundCell = result[cellId];
								cachedCellIds.splice(cachedCellIds.indexOf(cellId), 1);
							}

							//Add picture to cell
							foundCell.addPicture(pictures[i]);
						}
					}

					//Then save cells to cellstore
					console.log(result);
					for(let i=0; i < result.length; i++) {
						this.cellStore.update(result[i]);
					}

					//Fire event saying data is ready
					resolve(result);
				});
			}
			else {
				resolve(result);
			}
		});
	}

	/**
	 * Ask for statistics in a given area.
	 * You might listen to statscellready event to watch progress.
	 * @param bbox The area in which data must be retrieved
	 * @param {Object} [options] The options
	 * @param {long} [options.start] The start timestamp, in milliseconds since 1st january 1970.
	 * @param {long} [options.end] The end timestamp, in milliseconds since 1st january 1970.
	 * @param fresh Force download from the web (optional, false if undefined)
	 * @return {Promise} A promise resolving on statistics for all concerned cells.
	 */
	askStatistics(bbox, options, fresh) {
		return new Promise((resolve, reject) => {
			fresh = fresh || false;
			options = options || {};

			if(options.start) { options.start = this.roundDate(options.start); }
			else { options.start = this.roundDate(Date.now() - 6 * 30 * 24 * 60 * 60 * 1000); }

			if(options.end) { options.end = this.roundDate(options.end) + 24*60*60*1000; }
			else { options.end = null; }

			const cells = GridFactory.make(bbox, options);
			let cellsForFetchers = [];
			const rawStats = {};

			//Retrieve data from cache
			if(!fresh) {
				for(let cId in cells) {
					if(cells.hasOwnProperty(cId)) {
						let cell = cells[cId];
						let cellCache = this.cellStore.getStatistics(cell.bounds.toBBoxString(), options);

						//If cached, put in results
						if(cellCache !== null) {
							rawStats[cell.bounds.toBBoxString()] = cellCache;
							this.fire("statscellready", { bbox: cell.bounds.toBBoxString(), stats: cellCache });
						}
						//Else, add cell to fetcher download list
						else {
							cellsForFetchers.push(cell);
						}
					}
				}
			}
			else {
				this.cellStore.clear();
				cellsForFetchers = cells;
			}

			if(cellsForFetchers.length > 0) {
				const promises = [];

				for(const cell of cellsForFetchers) {
					promises.push(
						this.picMan.startSummaryRetrieval(cell.bounds, { mindate: options.start, maxdate: options.end, usefetchers: options.fetchers })
							.then(stats => {
								rawStats[cell.bounds.toBBoxString()] = stats;
								this.cellStore.updateStatistics(cell.bounds.toBBoxString(), options, stats);
								this.fire("statscellready", { bbox: cell.bounds.toBBoxString(), stats: stats });
							})
							.catch(e => console.warn(e))
					);
				}

				Promise.all(promises)
				.then(values => {
					resolve(rawStats);
				});
			}
			else {
				resolve(rawStats);
			}
		});
	}

	/**
	 * Converts a given timestamp into the timestamp of the same day, at 00:00
	 * @param {long} d The timestamp, in milliseconds
	 * @return {long} The timestamp of the same day at 00:00, in milliseconds
	 */
	roundDate(d) {
		return new Date((new Date(d)).toISOString().split('T')[0]).getTime();
	}
}

module.exports = DataManager;
