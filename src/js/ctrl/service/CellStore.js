/*
 * This file is part of Pic4Carto.
 * 
 * Pic4Carto is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * Pic4Carto is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Pic4Carto.  If not, see <http://www.gnu.org/licenses/>.
 */

let Cell = require("../../model/Cell");

/**
 * The cell store saves cells in browser local storage, and can retrieve them when needed.
 */
class CellStore {
//CONSTRUCTOR
	constructor() {
	}

//ACCESSORS
	/**
	 * @param {string} id The identifier of the cell to retrieve
	 * @return {Cell} The wanted cell, or null if it doesn't exist
	 */
	get(id) {
		let cell = localStorage.getItem("P4C-"+id);
		
		if(cell === null || cell.length == 0) { return null; }
		else {
			cell = JSON.parse(cell);
			let cellObj = new Cell(L.latLngBounds(
				L.latLng(cell.bounds._southWest.lat, cell.bounds._southWest.lng),
				L.latLng(cell.bounds._northEast.lat, cell.bounds._northEast.lng)
			));
			cellObj.creationDate = cell.creationDate;
			cellObj.pictures = cell.pictures;
			cellObj.options = cell.options;
			
			return cellObj;
		}
	}
	
	/**
	 * @param {string} bboxstring The string representation of a bounding box
	 * @param {Object} [cellOptions] The cell options
	 * @return {Object} The wanted cell statistics, or null if it doesn't exist
	 */
	getStatistics(bboxstring, cellOptions) {
		const id = this._cellStatId(bboxstring, cellOptions);
		
		let cell = localStorage.getItem(id);
		
		if(cell === null || cell.length == 0) { return null; }
		else {
			cell = cell.split("|");
			
			//Clear old values
			if(parseInt(cell[3]) < Date.now() - 6 * 60 * 60 * 1000) {
				localStorage.removeItem(id);
				return null;
			}
			//Return cached data
			else {
				return {
					last: parseInt(cell[0]),
					amount: parseInt(cell[1]),
					approxAmount: cell[2] === "true"
				};
			}
		}
	}

//MODIFIERS
	/**
	 * Changes the given cell in the store
	 * @param c The cell to save
	 * @param failsafe Set to true if called from catch
	 */
	update(c, failsafe) {
		failsafe = failsafe || false;
		
		try {
			localStorage.setItem("P4C-"+c.getId(), c.toJSONString());
		}
		catch(e) {
			console.log("[CellStore] Local storage full, removing cells");
			for(let i in localStorage) {
				if(/^P4C-Cell#/.test(i)) {
					localStorage.removeItem(i);
				}
			}
			
			if(!failsafe) {
				this.update(c, true);
			}
		}
	}
	
	/**
	 * Changes the given statistics in the store
	 * @param c The cell to save
	 * @param {Object} [cellOptions] The cell options
	 * @param stats The statistics object { amount: int, last: longint, approxAmount: bool }
	 * @param failsafe Set to true if called from catch
	 */
	updateStatistics(bboxstring, cellOptions, stats, failsafe) {
		const id = this._cellStatId(bboxstring, cellOptions);
		failsafe = failsafe || false;
		
		try {
			localStorage.setItem(id, stats.last+"|"+stats.amount+"|"+stats.approxAmount+"|"+Date.now());
		}
		catch(e) {
			console.log("[CellStore] Local storage full, removing cells statistics");
			for(let i in localStorage) {
				if(/^P4C-CellStat#/.test(i)) {
					localStorage.removeItem(i);
				}
			}
			
			if(!failsafe) {
				this.update(c, true);
			}
		}
	}
	
	/**
	 * Completely empties the cache storage
	 */
	clear() {
		localStorage.clear();
	}
	
	_cellStatId(bboxstring, cellOptions) {
		let id = "P4C-CellStat#"+bboxstring;
		for(const p in cellOptions) {
			id += "#"+p+"="+cellOptions[p];
		}
		return id;
	}
}

module.exports = CellStore;
