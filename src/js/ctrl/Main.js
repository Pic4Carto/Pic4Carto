/*
 * This file is part of Pic4Carto.
 * 
 * Pic4Carto is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * Pic4Carto is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Pic4Carto.  If not, see <http://www.gnu.org/licenses/>.
 */

require("./Compatibility");
const L = require("leaflet");
const $ = require("jquery");

const Cell = require("../model/Cell");
const DataManager = require("./service/DataManager");
const MainView = require("../view/Main");
const PicturesManager = require("pic4carto").PicturesManager;
const URLService = require("./service/URL");
const UDF = require("./UserDefinedFetchers");

const MIN_ZOOM_DATA = 14;

/**
 * Main controller manages everything related to main page.
 * In fact, it launches the Main view.
 */
class Main {
//CONSTRUCTOR
	constructor(divId) {
		this.view = new MainView(divId);
		this.dataManager = new DataManager();
		this.timer = null;
		
		//URL parameters
		this.url = new URLService(
			() => { return $(location).attr('href'); },
			(u) => { window.history.replaceState({}, "Pic4Carto", u); }
		);
		
		const cellOptions = this.url.getCellOptions();
		
		//Restore time filter
		const tStart = new Date(cellOptions.start ? cellOptions.start : Date.now() - 6 * 30 * 24 * 60 * 60 * 1000);
		const tEnd = cellOptions.end ? new Date(cellOptions.end) : null;
		
		this.view.areaSelector.mapTimeFilter.setDateRange(tStart, tEnd);
		
		//Restore provider filter
		this._setProvidersFromURL();
		
		//Manage timer for statistics retrieval
		this.view.areaSelector.map.on("load moveend zoomend", () => {
			if(this.timer !== null) {
				clearTimeout(this.timer);
			}
			
			this.timer = setTimeout(this.getStatistics.bind(this), 2000);
		});
		
		//Time filter updated
		this.view.areaSelector.mapTimeFilter.on("timefiltered", d => {
			if(d.from) { this.url.setParameter("from", d.from.getTime()); }
			else { this.url.setParameter("from", null); }
			
			if(d.to) { this.url.setParameter("to", d.to.getTime()); }
			else { this.url.setParameter("to", null); }
			
			this.url.write();
			this.getStatistics(true);
		});
		
		//Provider filter updated
		this.view.areaSelector.mapProviderFilter.on("providerfiltered", p => {
			const providerList = p.join(",");
			if(p.length > 0) {
				if(providerList !== this.url.getParameter("fetchers")) {
					this.url.setParameter("fetchers", (p.length < Object.keys((new PicturesManager({ userDefinedFetchers: UDF })).getFetcherDetails()).length) ? providerList : null);
					this.url.write();
					this.getStatistics(true);
				}
			}
			else {
				alert("You must select at least one provider !");
				this._setProvidersFromURL();
			}
		});
	}

//OTHER METHODS
	/**
	 * Ask for data retrieval to show map statistics
	 */
	getStatistics(paramChanged) {
		paramChanged = paramChanged || false;
		
		if(this.view.areaSelector.map.getZoom() >= MIN_ZOOM_DATA) {
			//Event for statistics retrieved
			this.dataManager.off("statscellready");
			this.dataManager.on("statscellready", d => {
				let west, south, east, north;
				[west, south, east, north] = d.bbox.split(',').map(parseFloat);
				this.view.areaSelector.setCellData(new Cell(new L.LatLngBounds(new L.LatLng(south, west), new L.LatLng(north, east))), d.stats, paramChanged);
			});
			
			//Ask for data retrieval
			this.view.areaSelector.setLoadingGridData(true);
			if(paramChanged) {
				this.view.areaSelector.resetCellData();
			}
			
			this.dataManager
			.askStatistics(
				this.view.areaSelector.map.getBounds(),
				{
					start: this.view.areaSelector.mapTimeFilter.getDateRange().from,
					end: this.view.areaSelector.mapTimeFilter.getDateRange().to,
					fetchers: this.view.areaSelector.mapProviderFilter.getProviders()
				},
				this.url.getParameter("refresh") == 1
			)
			.then(stats => {
				setTimeout(() => {
					this.view.areaSelector.setLoadingGridData(false);
				}, 500);
			});
		}
	}
	
	_setProvidersFromURL() {
		const providersFromURL = this.url.getCellOptions().fetchers;
		if(providersFromURL != null && providersFromURL.length > 0) {
			this.view.areaSelector.mapProviderFilter.setProviders(providersFromURL);
		}
	}
}

module.exports = {
	init: function(divId) {
		return new Main(divId);
	}
};
