const P4C = require("pic4carto");

module.exports = {
	fema_maria: {
		name: "FEMA (Maria)",
		logo: "https://upload.wikimedia.org/wikipedia/commons/thumb/6/67/FEMA_logo.svg/320px-FEMA_logo.svg.png",
		homepage: "https://www.fema.gov/",
		csv: "./csv/fema_maria.csv",
		license: "Public domain",
		user: "Federal Emergency Management Agency",
		bbox: new P4C.LatLngBounds(new P4C.LatLng(13.69, -67.28), new P4C.LatLng(18.55, -64.09))
	}
};
