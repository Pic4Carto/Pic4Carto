/*
 * This file is part of Pic4Carto.
 * 
 * Pic4Carto is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * Pic4Carto is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Pic4Carto.  If not, see <http://www.gnu.org/licenses/>.
 */

let $ = require("jquery");

let GridFactory = require("../model/GridFactory");
let URL = require("../ctrl/service/URL");

/**
 * WhatsNext view is a modal offering several cases to continue mapping in Pic4Carto.
 * It shows a map of adjacents cells, offers to go back to main map or just closes.
 */
class WhatsNext {
//CONSTRUCTOR
	constructor(divId, currentCell) {
		this.dom = $("#"+divId);
		this.dom.addClass("p4c-whatsnext");
		this.currentCell = currentCell;
		
		this.dom.append(
			"<span class=\"title\">This was the last picture in this area</span>"+
			"<span class=\"subtitle\">What do you want to do next ?</span>"+
			"<span class=\"option-title\">Browse a near area...</span>"+
			"<div class=\"option-area\">"+
				"<div id=\""+divId+"-cellmap\" class=\"cellmap\"></div>"+
			"</div>"+
			"<div class=\"option-title-elsewhere\"><span class=\"option-title\"><br />Or go elsewhere</span></div>"+
			"<div class=\"buttons\">"+
				"<button id=\""+divId+"-btn-main\">Go to main page</button></form>"+
				"<button id=\""+divId+"-btn-close\">Return to photos in this area</button>"+
			"</div>"
		);
		
		$("#"+divId+"-btn-close").click(this.hide.bind(this));
		$("#"+divId+"-btn-main").click(() => { window.location = "./?"+URL.cellOptionsToQueryPart(this.currentCell)+"#15/"+this.currentCell.bounds.getCenter().lat+"/"+this.currentCell.bounds.getCenter().lng; });
		
		//Map init
		this.map = L.map(divId+"-cellmap", { dragging: false, zoomControl: false, doubleClickZoom: false, scrollWheelZoom: false }).setView([42.5, -3], 3);
		
		var http = window.location && window.location.protocol == 'https:' ? 'https:' : 'http:';
		L.tileLayer(http+'//{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
			name: "OpenStreetMap",
			attribution: "Tiles <a href=\"http://openstreetmap.org/\">OSM</a>",
			maxZoom: 19
		}).addTo(this.map);
		
		/*
		 * Show adjacent cells
		 */
		this.map.invalidateSize();
		this.cellLayer = L.featureGroup();
		this.cellLayer.addTo(this.map);
	}

//MODIFIERS
	/**
	 * Shows the modal
	 */
	show() {
		this.dom.addClass("visible");
		
		this.map.invalidateSize();
		this.map.fitBounds(this.currentCell.bounds.pad(0.2));
		
		setTimeout(() => {
			let bounds = this.map.getBounds();
			if(bounds.getEast() - bounds.getWest() > 0.1) {
				bounds = this.currentCell.bounds.pad(0.2);
			}
			
			let cells = GridFactory.make(bounds);
			this.cellLayer.clearLayers();
			
			for(let cellId=0; cellId < cells.length; cellId++) {
				let cell = cells[cellId];
				if(!cell.bounds.equals(this.currentCell.bounds)) {
					let myCellBoundsString = cell.bounds.toBBoxString();
					this.cellLayer.addLayer(
						L.rectangle(
							cell.bounds
						)
						.on("click", () => { window.location = "player.html?bbox="+myCellBoundsString+"&"+URL.cellOptionsToQueryPart(this.currentCell); })
					);
				}
				else {
					let myCellBoundsString = cell.bounds.toBBoxString();
					this.cellLayer.addLayer(
						L.rectangle(
							cell.bounds,
							{
								fillColor: "red",
								fillOpacity: 0.5
							}
						)
						.on("click", () => { window.location = "player.html?bbox="+myCellBoundsString+"&"+URL.cellOptionsToQueryPart(this.currentCell); })
					);
				}
			}
			
			this.map.fitBounds(this.currentCell.bounds.pad(0.2));
		}, 100);
	}
	
	/**
	 * Hides the modal
	 */
	hide() {
		this.dom.removeClass("visible");
	}
}

module.exports = WhatsNext;
