/*
 * This file is part of Pic4Carto.
 * 
 * Pic4Carto is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * Pic4Carto is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Pic4Carto.  If not, see <http://www.gnu.org/licenses/>.
 */

let $ = require("jquery");

/**
 * The progress view shows the progress made for a given task.
 */
class Progress {
//CONSTRUCTOR
	constructor(divId) {
		this.dom = $("#"+divId);
		this.text = null;
		
		//Create DIV structure
		this.dom.empty();
		this.dom.addClass("p4c-progress");
		this.dom.append("<span class=\"text\" ></span>");
		this.dom.append("<span class=\"value\" ></span>");
		
		this.set(0);
	}

//MODIFIERS
	/**
	 * Change the current progress value
	 * @param val {int} The progress, in % (0 to 100)
	 */
	set(val) {
		this.value = val;
		if(val == 0) { val = 1; }
		this.dom.find(".value").css("width", val+"%");
		this.setText(this.text);
	}
	
	/**
	 * Change the custom text value
	 * @param val {string} The text to add
	 */
	setText(val) {
		this.text = val;
		if(this.text != null && this.text.length > 0) {
			this.dom.find(".text").text(this.value+" % ("+this.text+")");
		}
		else {
			this.dom.find(".text").text(this.value+" %");
		}
	}
}

module.exports = Progress;
