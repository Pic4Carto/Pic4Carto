/*
 * This file is part of Pic4Carto.
 * 
 * Pic4Carto is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * Pic4Carto is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Pic4Carto.  If not, see <http://www.gnu.org/licenses/>.
 */

const L = require("leaflet");
require("leaflet-geosearch");
require("leaflet-geosearch/src/js/l.geosearch.provider.openstreetmap");
require("leaflet-area-select");
require("leaflet-easybutton");
require("../lib/leaflet-messagebox");
require("./TimeFilter");
require("./ProviderFilter");

const GridFactory = require("../model/GridFactory");
const GridExporter = require("../ctrl/service/GridExporter");
const PicturesManager = require("pic4carto").PicturesManager;
const UDF = require("../ctrl/UserDefinedFetchers");

/**
 * AreaSelector view permits for user to select a given area.
 * Two modes are available : grid of free. Grid shows an existing grid and user selects one cell. Free let user select whatever area he wants.
 */
class AreaSelector {
//CONSTRUCTOR
	/**
	 * Class constructor
	 * @param divId The DIV where this view will be created
	 * @param mode The area select mode (grid, free)
	 * @param minzoom The minimal zoom to show selectors
	 * @param callback The callback function when user has selected its area
	 */
	constructor(divId, mode, minzoom, callback) {
		this.mode = mode;
		this.minzoom = minzoom;
		this.callback = callback;
		
		//Map init
		this.map = L.map(divId);
		this.mapMsg = L.control.messagebox({ timeout: -1 }).addTo(this.map);
		this.mapGrid = null;
		this.mapCells = null;
		this.mapSearch = new L.Control.GeoSearch({
			provider: new L.GeoSearch.Provider.OpenStreetMap(),
			position: 'topleft',
			showMarker: false,
			retainZoomLevel: false
		}).addTo(this.map);
		
		var http = window.location && window.location.protocol == 'https:' ? 'https:' : 'http:';
		L.tileLayer(http+'//{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
			name: "OpenStreetMap",
			attribution: "Tiles <a href=\"http://openstreetmap.org/\">OSM</a>",
			maxZoom: 19
		}).addTo(this.map);
		
		//Zoom level enables/disables selector
		this.selectorEnabled = false;
		this.map.on("load zoomend", () => {
			if(this.map.getZoom() >= this.minzoom && !this.selectorEnabled) {
				this.mapMsg.hide();
				this.enableSelector();
			}
			if(this.map.getZoom() < this.minzoom) {
				this.disableSelector();
				this.mapMsg.show("Zoom in to select area");
			}
		});
		
		//Time filter
		this.mapTimeFilter = L.control.timeFilter({ position: "topleft" });
		this.mapTimeFilter.addTo(this.map);
		
		//Provider filter
		this.mapProviderFilter = L.control.providerFilter({
												providers: (new PicturesManager({ userDefinedFetchers: UDF })).getFetcherDetails(),
												position: "topleft"
											});
		this.mapProviderFilter.addTo(this.map);
		
		//Export buttons
		this.mapBtnExport = L.easyButton('<img src="img/export.32.png" alt="E" />', this.showExportButtons.bind(this), "Export grid").addTo(this.map);
		this.mapBtnExport.disable();
		
		this.mapBtnExporters = L.easyBar([
			L.easyButton('<span>G</span>', (control) => {
				this.exportGrid(GridExporter.exportAsGeoJSON);
			}, "Export as GeoJSON"),
			L.easyButton('<span>O</span>', (control) => {
				this.exportGrid(GridExporter.exportAsOSMXML);
			}, "Export as OSM XML")
		]);
		
		this.map.setView([42.5, -3], 3);
	}

//MODIFIERS
	/**
	 * Enable the selector system
	 */
	enableSelector() {
		this.selectorEnabled = true;
		if(this.mode == "grid") {
			this.mapMsg.show("Click on a cell to start");
			this.showGrid();
			this.mapBtnExport.enable();
			
			//Update grid when map moves
			this.map.on("moveend", () => {
				if(this.selectorEnabled) {
					this.hideExportButtons();
					this.removeGrid();
					this.showGrid();
				}
			});
		}
		else if(this.mode == "free") {
			this.mapMsg.show("Press Ctrl and drag<br />an area on the map to start");
			this.map.selectArea.enable();
			this.map.selectArea.setControlKey(true);
			this.map.off('areaselected');
			this.map.on('areaselected', (e) => {
				this.callback(e.bounds, {
					date: this.mapTimeFilter.getDateRange(),
					fetchers: this.mapProviderFilter.getProviders()
				});
			});
		}
	}
	
	/**
	 * Disable the selector system
	 */
	disableSelector() {
		this.selectorEnabled = false;
		if(this.mode == "grid") {
			this.removeGrid();
			this.mapBtnExport.disable();
		}
		else if(this.mode == "free") {
			this.map.selectArea.disable();
		}
	}
	
	/**
	 * Show a square grid as map layer
	 */
	showGrid() {
		this.map.invalidateSize(false);
		let bbox = this.map.getBounds();
		let cells = GridFactory.make(bbox);
		if(this.mapGrid === null) {
			this.mapGrid = L.layerGroup();
			this.mapGrid.addTo(this.map);
			this.mapCells = {};
			this.alreadySetCells = [];
		}
		
		//Add cells in layer group
		for(let cId=0; cId < cells.length; cId++) {
			let myBounds = cells[cId].bounds;
			
			//Create default map cell if no one defined
			if(this.mapCells[myBounds.toBBoxString()] === undefined) {
				this.mapCells[myBounds.toBBoxString()] = L.rectangle(myBounds).on("click", () => {
					this.callback(myBounds, {
						date: this.mapTimeFilter.getDateRange(),
						fetchers: this.mapProviderFilter.getProviders()
					});
				});
			}
			this.mapGrid.addLayer(this.mapCells[myBounds.toBBoxString()]);
		}
		
		this.lastCells = cells;
	}
	
	/**
	 * Clears set data on cell.
	 */
	resetCellData() {
		if(this.map.getZoom() >= this.minzoom && this.selectorEnabled && this.mode == "grid") {
			this.removeGrid();
			this.mapGrid = null;
			this.showGrid();
		}
	}
	
	/**
	 * Defines some data associated to a grid cell
	 * @param c {Cell} The cell
	 * @param d {object} The data to associate { last, approxAmount, amount }
	 * @param force {boolean} Force redefine even if already set
	 */
	setCellData(c, d, force) {
		force = force || false;
		
		if(!force && this.alreadySetCells.indexOf(c.getId()) >= 0) {
			return false;
		}
		
		this.alreadySetCells.push(c.getId());
		let bounds = c.bounds;
		let layer = L.featureGroup();
		layer.on("click", () => {
			this.callback(bounds, {
				date: this.mapTimeFilter.getDateRange(),
				fetchers: this.mapProviderFilter.getProviders()
			});
		});
		
		let style = {
			fillOpacity: 0.7,
			color: "#333",
			opacity: 0.5
		};
		
		//Cell color (amount of pictures)
		if(d.amount == 0) {
			style.fillColor = "#fee5d9";
			style.fillOpacity = 0.4;
		}
		else if(d.amount < 25) {
			style.fillColor = "#fcae91";
		}
		else if(d.amount < 100) {
			style.fillColor = "#fb6a4a";
		}
		else {
			style.fillColor = "#cb181d";
		}
		
		layer.addLayer(L.rectangle(bounds, style));
		
		//Cell text (amount of pictures, last picture date)
		let lastDate = (d.last) ? new Date(d.last) : null;
		let text = (d.approxAmount) ? '>' : '';
		let day = (new Date(lastDate)).toISOString().split('T')[0].replace(/^[0-9]{2}([0-9]{2})-([0-9]{2})-([0-9]{2})$/, "$3/$2/$1");
		text += (d.amount > 0) ? d.amount + '<br />pics<br />'+day : 'No recent<br />picture';
		
		layer.addLayer(L.marker(
			bounds.getCenter(),
			{
				icon: L.divIcon({
					className: 'p4c-areaselector-cellstat',
					html: text,
					iconSize: [50,50],
					iconAnchor: [25,25]
				})
			}
		));
		
		if(this.mapCells[bounds.toBBoxString()] !== undefined) {
			this.mapGrid.removeLayer(this.mapCells[bounds.toBBoxString()]);
		}
		this.mapCells[bounds.toBBoxString()] = layer;
		this.mapGrid.addLayer(this.mapCells[bounds.toBBoxString()]);
	}
	
	/**
	 * Shows that grid statistics is loading
	 * @param loading True to show, false to hide
	 */
	setLoadingGridData(loading) {
		if(loading) {
			this.mapMsg.show("Click on a cell to start<br />Loading statistics...");
		}
		else {
			this.mapMsg.show("Click on a cell to start");
		}
	}
	
	/**
	 * Removes an already added square grid
	 */
	removeGrid() {
		if(this.mapGrid !== null) {
			this.mapGrid.clearLayers();
		}
		if(this.lastCells != null) {
			this.lastCells = null;
		}
	}
	
	/**
	 * Export the current grid
	 * @param exporter The export function
	 */
	exportGrid(exporter) {
		if(this.selectorEnabled && this.mapGrid !== null) {
			exporter(GridFactory.make(this.map.getBounds()));
		}
		else {
			alert("Grid not available");
		}
		this.hideExportButtons();
	}
	
	/**
	 * Show the buttons to choose which exporter to use
	 */
	showExportButtons() {
		this.map.removeControl(this.mapBtnExport);
		this.mapBtnExporters.addTo(this.map);
	}
	
	/**
	 * Hide the buttons to choose which exporter to use
	 */
	hideExportButtons() {
		this.map.removeControl(this.mapBtnExporters);
		this.mapBtnExport.addTo(this.map);
	}
}

module.exports = AreaSelector;
