/*
 * This file is part of Pic4Carto.
 * 
 * Pic4Carto is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * Pic4Carto is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Pic4Carto.  If not, see <http://www.gnu.org/licenses/>.
 */

const $ = require("jquery");
const L = require("leaflet");
require("leaflet-hash");

const AreaSelector = require("./AreaSelector");
const URL = require("../ctrl/service/URL");
const PicturesManager = require("pic4carto").PicturesManager;
const UDF = require("../ctrl/UserDefinedFetchers");

/**
 * Main view is the view manager associated to main page.
 * It creates the DOM structure and associates area selectors.
 */
class Main {
//CONSTRUCTOR
	constructor(divId) {
		this.dom = $("#"+divId);
		this.dom.append("<div id=\"selector-grid\" class=\"selector\"></div>");
		
		//Init selectors
		let handler = (bbox, options) => {
			window.location = "player.html?bbox="+bbox.toBBoxString()
									+((options.date.from) ? "&from="+options.date.from.getTime() : "")
									+((options.date.to) ? "&to="+options.date.to.getTime() : "")
									+((
										options.fetchers
										&& options.fetchers.length < Object.keys((new PicturesManager({ userDefinedFetchers: UDF })).getFetcherDetails()).length
									) ? "&fetchers="+options.fetchers.join(",") : "");
		};
		
		this.areaSelector = new AreaSelector("selector-grid", "grid", 14, handler);
		let hash = new L.Hash(this.areaSelector.map);
	}
}

module.exports = Main;
