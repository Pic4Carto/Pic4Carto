/*
 * This file is part of Pic4Carto.
 * 
 * Pic4Carto is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * Pic4Carto is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Pic4Carto.  If not, see <http://www.gnu.org/licenses/>.
 */

let $ = require("jquery");

/**
 * Messages manages a queue of messages, and display them.
 */
class Messages {
//CONSTRUCTOR
	constructor(divId) {
		this.dom = $("#"+divId);
		this.queue = [];
		this.timer = null;
		
		//Create DIV structure
		this.dom.empty();
		this.dom.addClass("p4c-messages hidden");
		this.dom.append("<div class=\"content\"></div>");
	}

//MODIFIERS
	/**
	 * Display the given message after all existing messages in queue
	 * @param message The text to display
	 * @param type The kind of message (info, alert, error), can be used for CSS
	 * @param timeout The duration of the message display
	 */
	show(message, type, timeout) {
		this.queue.push({ msg: message, type: type, timeout: timeout });
		if(this.timer === null || this.timer === -1) {
			this.showNext();
		}
	}
	
	/**
	 * Handles changing the current message
	 */
	showNext() {
		if(this.queue.length > 0) {
			this.dom.removeClass("hidden");
			
			let message = this.queue.shift();
			this.dom
				.removeClass("info alert error")
				.addClass(message.type);
			
			this.dom
				.find(".content")
				.html(message.msg);
			
			if(message.timeout > 0) {
				this.timer = setTimeout(() => {
						this.showNext()
					},
					message.timeout
				);
			}
			else {
				this.timer = -1;
			}
		}
		else {
			this.hide();
		}
	}
	
	/**
	 * Hides the message
	 */
	hide() {
		this.dom.addClass("hidden");
		this.timer = null;
	}
}

module.exports = Messages;
