/**
 * Expected behaviour: show modal, hide, then show again
 */

let L = require("leaflet");
let Cell = require("../../../src/js/model/Cell");
let WhatsNext = require("../../../src/js/view/WhatsNext");

let wn1 = new WhatsNext("whatsnext", new Cell(L.latLngBounds(L.latLng(48.125, -1.68), L.latLng(48.13, -1.675))));

setTimeout(() => { wn1.show(); }, 1000);
setTimeout(() => { wn1.hide(); }, 2000);
setTimeout(() => { wn1.show(); }, 3000);
