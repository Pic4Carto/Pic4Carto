/**
 * Expected behaviour: show three sources, first successes, second fails, third successes
 */

let Loading = require("../../../src/js/view/Loading");

let l1 = new Loading(
			"loading",
			{
				"mapillary": {
					"name": "Mapillary",
					"logoUrl": "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a3/Mapillary_logo.svg/480px-Mapillary_logo.svg.png"
				},
				"wikicommons": {
					"name": "Wikimedia Commons",
					"logoUrl": "https://commons.wikimedia.org/static/images/project-logos/commonswiki.png",
				},
				"flickr": {
					"name": "Flickr",
					"logoUrl": "https://upload.wikimedia.org/wikipedia/commons/thumb/5/52/Flickr_wordmark.svg/640px-Flickr_wordmark.svg.png"
				},
				"openstreetcam": {
					"name": "OpenStreetCam",
					"logoUrl": "http://openstreetcam.org/images/osc-logo.png"
				}
			}
		);

l1.on("completed", () => { alert("Loading done !!"); });
l1.on("skip", () => { alert("User wants to skip loading"); });

setTimeout(() => { l1.done("mapillary"); }, 2000);
setTimeout(() => { l1.failed("wikicommons"); }, 6000);
setTimeout(() => { l1.done("flickr"); }, 10000);
