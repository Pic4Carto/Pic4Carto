/*
 * Expected behaviour: show a picture description, then another one
 */

let L = require("leaflet");
let Description = require("../../../src/js/view/Description");
let Picture = require("../../../src/js/model/Picture");

let d = new Description("description");
d.set(new Picture("http://test.net/img.jpg", 123456789, L.latLng(10.1, 48.7), "ProviderPics", "PanierAvide", "CC By-SA 4.0", "http://test.net/img_details.html", 175));

setTimeout(() => {
		d.set(new Picture("http://test.net/img2.jpg", 987654321, L.latLng(10.1, 48.7), "AnotherProv", "SomeOne", "WTFPL 1.0", "http://test.net/img2_details.html", 175));
	},
	2000
);
