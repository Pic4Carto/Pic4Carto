/*
 * Expected behaviour: show time filter, and display alert when time range changed
 */

let L = require("leaflet");
let P4C = require("pic4carto");
let PicturesManager = P4C.PicturesManager;
require("../../../src/js/view/ProviderFilter");

//Map init
let map = L.map("map").setView(L.latLng(48.11, -1.65), 12);
L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
	name: "OpenStreetMap",
	attribution: "Tiles <a href=\"http://openstreetmap.org/\">OSM</a>",
	maxZoom: 19
}).addTo(map);

let pf = L.control.providerFilter({ providers: (new PicturesManager()).getFetcherDetails() });
pf.addTo(map);

//All selected
console.log(pf.getProviders());

setTimeout(function() {
	pf.setProviders([ "flickr", "wikicommons" ]);
	console.log(pf.getProviders());
}, 1000);

pf.on("providerfiltered", function(providers) {
	console.log("Changed", providers);
});
