/**
 * Test script for Cell
 */

let QUnit = require("qunitjs");
let L = require("leaflet");
let Cell = require("../../../src/js/model/Cell");
let Picture = require("../../../src/js/model/Picture");

QUnit.module("Model > Cell");


/*
 * CONSTRUCTOR
 */
QUnit.test("Constructor with valid parameters", function(assert) {
	let c1 = new Cell(L.latLngBounds(L.latLng(10, 1), L.latLng(11, 2)), { start: 1488991013000, end: 1488991016000});
	
	//Check attributes
	assert.ok(c1.bounds.equals(L.latLngBounds(L.latLng(10, 1), L.latLng(11, 2))));
	assert.equal(Math.round(c1.creationDate / 1000), Math.round(Date.now() / 1000));
	assert.equal(c1.pictures.length, 0);
	assert.equal(c1.options.start, 1488991013000);
	assert.equal(c1.options.end, 1488991016000);
});

QUnit.test("Constructor with missing parameters", function(assert) {
	assert.throws(
		() => { let c1 = new Cell(); },
		new Error("model.cell.invalidbounds")
	);
	
	assert.throws(
		() => { let c1 = new Cell({}); },
		new Error("model.cell.invalidbounds")
	);
});


/*
 * ACCESSORS
 */

//getId
QUnit.test("getId", function(assert) {
	let c1 = new Cell(L.latLngBounds(L.latLng(10.123, -1.123), L.latLng(11.456, -1.098)), { start: 1488991013000, end: 1488991016000});
	assert.equal(c1.getId(), "Cell#-1.123,10.123,-1.098,11.456#start=1488991013000#end=1488991016000");
});

QUnit.test("getId without start time", function(assert) {
	let c1 = new Cell(L.latLngBounds(L.latLng(10.123, -1.123), L.latLng(11.456, -1.098)), { end: 1488991016000 });
	assert.equal(c1.getId(), "Cell#-1.123,10.123,-1.098,11.456#end=1488991016000");
});

QUnit.test("getId without end time", function(assert) {
	let c1 = new Cell(L.latLngBounds(L.latLng(10.123, -1.123), L.latLng(11.456, -1.098)), { start: 1488991013000 });
	assert.equal(c1.getId(), "Cell#-1.123,10.123,-1.098,11.456#start=1488991013000");
});

//getPictures
QUnit.test("getPictures with no pictures", function(assert) {
	let c1 = new Cell(L.latLngBounds(L.latLng(10.123, -1.123), L.latLng(11.456, -1.098)));
	let result = c1.getPictures();
	assert.equal(result.length, 0);
});

QUnit.test("getPictures with several pictures", function(assert) {
	let c1 = new Cell(L.latLngBounds(L.latLng(10.123, -1.123), L.latLng(11.456, -1.098)));
	
	c1.addPicture(new Picture("http://test.net/img.jpg", 1, L.latLng(10.1, 48.7), "Myself", "Me", "My license", "http://test.net/img_details.html", 175));
	c1.addPicture(new Picture("http://test.net/img.jpg", 3, L.latLng(10.1, 48.7), "Myself", "Me", "My license", "http://test.net/img_details.html", 175));
	c1.addPicture(new Picture("http://test.net/img.jpg", 2, L.latLng(10.1, 48.7), "Myself", "Me", "My license", "http://test.net/img_details.html", 175));
	
	let result = c1.getPictures();
	assert.equal(result.length, 3);
	assert.equal(result[0].date, 3);
	assert.equal(result[1].date, 2);
	assert.equal(result[2].date, 1);
});

//toJSONString
QUnit.test("toJSONString", function(assert) {
	let c1 = new Cell(L.latLngBounds(L.latLng(10.123, -1.123), L.latLng(11.456, -1.098)), { start: 1488991013000, end: 1488991016000 });
	c1.addPicture(new Picture("http://test.net/img.jpg", 123456789, L.latLng(10.1, 48.7), "Myself", "Me", "My license", "http://test.net/img_details.html", 175));
	
	assert.equal(c1.toJSONString(), "{\"creationDate\":"+c1.creationDate+",\"pictures\":[{\"pictureUrl\":\"http://test.net/img.jpg\",\"date\":123456789,\"coordinates\":{\"lat\":10.1,\"lng\":48.7},\"provider\":\"Myself\",\"author\":\"Me\",\"license\":\"My license\",\"detailsUrl\":\"http://test.net/img_details.html\",\"direction\":175}],\"bounds\":{\"_southWest\":{\"lat\":10.123,\"lng\":-1.123},\"_northEast\":{\"lat\":11.456,\"lng\":-1.098}},\"options\":{\"start\":1488991013000,\"end\":1488991016000}}");
});


/*
 * MODIFIERS
 */

//addPicture
QUnit.test("addPicture", function(assert) {
	let c1 = new Cell(L.latLngBounds(L.latLng(10.123, -1.123), L.latLng(11.456, -1.098)));
	
	assert.equal(c1.pictures.length, 0);
	
	c1.addPicture(new Picture("http://test.net/img.jpg", 123456789, L.latLng(10.1, 48.7), "Myself", "Me", "My license", "http://test.net/img_details.html", 175));
	
	assert.equal(c1.pictures.length, 1);
	assert.equal(c1.pictures[0].pictureUrl, "http://test.net/img.jpg");
});
