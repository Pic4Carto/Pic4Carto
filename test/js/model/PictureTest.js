/*
 * Test script for model/Picture.js
 */

let QUnit = require("qunitjs");
let L = require("leaflet");
let Picture = require("../../../src/js/model/Picture");

QUnit.module("Model > Picture");


/*
 * CONSTRUCTOR
 */
QUnit.test("Constructor with valid parameters", function(assert) {
	let p1 = new Picture("http://test.net/img.jpg", 123456789, L.latLng(10.1, 48.7), "Myself", "Me", "My license", "http://test.net/img_details.html", 175);
	
	//Check attributes
	assert.equal(p1.pictureUrl, "http://test.net/img.jpg");
	assert.equal(p1.date, 123456789);
	assert.ok(p1.coordinates.equals(L.latLng(10.1, 48.7)));
	assert.equal(p1.provider, "Myself");
	assert.equal(p1.author, "Me");
	assert.equal(p1.license, "My license");
	assert.equal(p1.detailsUrl, "http://test.net/img_details.html");
	assert.equal(p1.direction, 175);
});

QUnit.test("Constructor with valid parameters (only mandatory ones)", function(assert) {
	let p1 = new Picture("http://test.net/img.jpg", 123456789, L.latLng(10.1, 48.7), "Myself");
	
	//Check attributes
	assert.equal(p1.pictureUrl, "http://test.net/img.jpg");
	assert.equal(p1.date, 123456789);
	assert.ok(p1.coordinates.equals(L.latLng(10.1, 48.7)));
	assert.equal(p1.provider, "Myself");
	assert.equal(p1.author, null);
	assert.equal(p1.license, null);
	assert.equal(p1.detailsUrl, null);
	assert.equal(p1.direction, null);
});

QUnit.test("Constructor with missing mandatory parameters", function(assert) {
	assert.throws(
		function() { 
			let p1 = new Picture();
		},
		new Error("model.picture.invalid.pictureUrl")
	);
	
	assert.throws(
		function() { 
			let p1 = new Picture("http://test.net/img.jpg");
		},
		new Error("model.picture.invalid.date")
	);
	
	assert.throws(
		function() { 
			let p1 = new Picture("http://test.net/img.jpg", 123456789);
		},
		new Error("model.picture.invalid.coords")
	);
	
	assert.throws(
		function() { 
			let p1 = new Picture("http://test.net/img.jpg", 123456789, L.latLng(10, 20));
		},
		new Error("model.picture.invalid.provider")
	);
});

QUnit.test("Constructor with invalid parameters types", function(assert) {
	assert.throws(
		function() { 
			let p1 = new Picture(123456, 123456789, L.latLng(10.1, 48.7), "Myself", "Me", "My license", "http://test.net/img_details.html", 175);
		},
		new Error("model.picture.invalid.pictureUrl")
	);
	
	assert.throws(
		function() { 
			let p1 = new Picture("http://test.net/img.jpg", "lol", L.latLng(10.1, 48.7), "Myself", "Me", "My license", "http://test.net/img_details.html", 175);
		},
		new Error("model.picture.invalid.date")
	);
	
	assert.throws(
		function() { 
			let p1 = new Picture("http://test.net/img.jpg", 123456789, 42, "Myself", "Me", "My license", "http://test.net/img_details.html", 175);
		},
		new Error("model.picture.invalid.coords")
	);
	
	assert.throws(
		function() { 
			let p1 = new Picture("http://test.net/img.jpg", 123456789, L.latLng(10.1, 48.7), 42, "Me", "My license", "http://test.net/img_details.html", 175);
		},
		new Error("model.picture.invalid.provider")
	);
	
	let p1 = new Picture("http://test.net/img.jpg", 123456789, L.latLng(10.1, 48.7), "Myself", "Me", "My license", "http://test.net/img_details.html", "north");
	assert.equal(p1.direction, null);
});
