/**
 * Test script for DataManager
 */

const L = require("leaflet");
const QUnit = require("qunitjs");
const DataManager = require("../../../../src/js/ctrl/service/DataManager");

QUnit.module("Ctrl > Service > DataManager");

/*
 * OTHER METHODS
 */

//ask
QUnit.test("ask fresh over not empty area", function(assert) {
	const bbox = L.latLngBounds(L.latLng(48.1040,-1.6915), L.latLng(48.1182,-1.6658));
	const dm1 = new DataManager();
	const done = assert.async();
	
	dm1.ask(bbox, null, true)
	.then(cells => {
// 		console.log("Cells: "+cells.length);
		assert.ok(cells.length > 0);
		let noPicturesCell = 0;
		
		for(let i=0; i < cells.length; i++) {
			let cell = cells[i];
			if(cell.pictures.length == 0) { noPicturesCell++; }
			assert.ok(cell.creationDate >= Date.now() - 6 * 60 * 60 * 1000);
		}
		assert.ok(noPicturesCell < cells.length / 2);
		
		setTimeout(() => { done(); }, 1000);
	})
	.catch(e => {
		assert.fail(e);
		done();
	});
});

QUnit.test("ask fresh over not empty area and with fixed time range", function(assert) {
	let bbox = L.latLngBounds(L.latLng(48.1040,-1.6915), L.latLng(48.1182,-1.6658));
	let dm1 = new DataManager();
	let done = assert.async();
	
	dm1.ask(bbox, { start: 1464739200123, end: 1483228799000}, true)
	.then(cells => {
		assert.ok(cells.length > 0);
		let noPicturesCell = 0;
		
		for(let i=0; i < cells.length; i++) {
			let cell = cells[i];
			if(cell.pictures.length == 0) { noPicturesCell++; }
			else {
				for(let p of cell.pictures) {
					assert.ok(p.date >= 1464739200000);
					assert.ok(p.date <= 1483228800000);
				}
			}
			assert.equal(cell.options.start, 1464739200000);
			assert.equal(cell.options.end, 1483228800000);
			assert.ok(cell.creationDate >= Date.now() - 6 * 60 * 60 * 1000);
		}
		assert.ok(noPicturesCell < cells.length / 3);
		
		setTimeout(() => { done(); }, 1000);
	})
	.catch(e => {
		assert.fail(e);
		done();
	});
});

QUnit.test("ask cached over not empty area", function(assert) {
	let bbox = L.latLngBounds(L.latLng(48.1040,-1.6915), L.latLng(48.1182,-1.6658));
	let dm1 = new DataManager();
	let done = assert.async();
	
	dm1.ask(bbox, null, true)
	.then(cells => {
		assert.ok(cells.length > 0);
		
		//Now, ask using possibly cached data
		dm1.ask(bbox)
		.then(cells => {
// 			console.log("Cells: "+cells.length);
			assert.ok(cells.length > 0);
			let noPicturesCell = 0;
			
			for(let i=0; i < cells.length; i++) {
				let cell = cells[i];
				if(cell.pictures.length == 0) { noPicturesCell++; }
				assert.ok(cell.creationDate >= Date.now() - 6 * 60 * 60 * 1000);
			}
			assert.ok(noPicturesCell < cells.length / 2);
			
			setTimeout(() => { done(); }, 1000);
		})
		.catch(e => {
			assert.fail(e);
			done();
		});
	})
	.catch(e => {
		assert.fail(e);
		done();
	});
});

QUnit.test("ask cached over not empty area with time range defined", function(assert) {
	let bbox = L.latLngBounds(L.latLng(48.1040,-1.6915), L.latLng(48.1182,-1.6658));
	let dm1 = new DataManager();
	let done = assert.async();
	
	dm1.ask(bbox, { start: 1464739200123, end: 1483228799000 }, true)
	.then(cells => {
		assert.ok(cells.length > 0);
		
		//Now, ask using possibly cached data
		dm1.ask(bbox, { start: 1464739200123, end: 1483228799000 })
		.then(cells => {
			// 			console.log("Cells: "+cells.length);
			assert.ok(cells.length > 0);
			let noPicturesCell = 0;
			
			for(let i=0; i < cells.length; i++) {
				let cell = cells[i];
				if(cell.pictures.length == 0) { noPicturesCell++; }
				else {
					for(let p of cell.pictures) {
						assert.ok(p.date >= 1464739200000);
						assert.ok(p.date <= 1483228800000);
					}
				}
				assert.equal(cell.options.start, 1464739200000);
				assert.equal(cell.options.end, 1483228800000);
				assert.ok(cell.creationDate >= Date.now() - 6 * 60 * 60 * 1000);
			}
			assert.ok(noPicturesCell < cells.length / 3);
			
			setTimeout(() => { done(); }, 1000);
		})
		.catch(e => {
			assert.fail(e);
			done();
		});
	})
	.catch(e => {
		assert.fail(e);
		done();
	});
});

QUnit.test("ask fresh over empty area", function(assert) {
	let bbox = L.latLngBounds(L.latLng(35.75, -44.78), L.latLng(35.76, -44.77));
	let dm1 = new DataManager();
	let done = assert.async();
	
// 	dm1.on("fetcherdone", f => { console.log("Fetcher "+f+" done"); });
// 	dm1.on("fetcherfailed", f => { console.log("Fetcher "+f+" failed"); });
	
	dm1.ask(bbox, null, true)
	.then(cells => {
// 		console.log("Cells: "+cells.length);
		assert.ok(cells.length > 0);
		
		for(let i=0; i < cells.length; i++) {
			let cell = cells[i];
			assert.equal(cell.pictures.length, 0);
			assert.ok(cell.creationDate >= Date.now() - 6 * 60 * 60 * 1000);
		}
		
		setTimeout(() => { done(); }, 1000);
	})
	.catch(e => {
		assert.fail(e);
		done();
	});
});

QUnit.test("ask with events for watching progress", function(assert) {
	const bbox = L.latLngBounds(L.latLng(48.1040,-1.6915), L.latLng(48.1182,-1.6658));
	const dm1 = new DataManager();
	const done = assert.async();
	
	let fetcherfail = 0;
	let fetcherok = 0;
	
	dm1.on("fetcherfailed", fid => {
		fetcherfail++;
	});
	
	dm1.on("fetcherdone", fid => {
		fetcherok++;
	});
	
	dm1.ask(bbox, { fetchers: [ "mapillary" ] }, true)
	.then(cells => {
		assert.ok(cells.length > 0);
		setTimeout(() => {
			assert.equal(fetcherfail, 0);
			assert.equal(fetcherok, 1);
			done();
		}, 1000);
	})
	.catch(e => {
		assert.fail(e);
		done();
	});
});

//askStatistics
QUnit.test("askStatistics fresh over not empty area", function(assert) {
	let bbox = L.latLngBounds(L.latLng(48.1040,-1.6915), L.latLng(48.1182,-1.6658));
	let dm1 = new DataManager();
	let done = assert.async();
	let nbCellsReady = 0;
	
	dm1.on("statscellready", (d) => {
		assert.ok(/^-?[0-9]+(\.[0-9]+)?(,-?[0-9]+(\.[0-9]+)?){3}$/.test(d.bbox));
		assert.ok(!isNaN(d.stats.amount));
		assert.ok(d.stats.approxAmount !== undefined);
		if(d.stats.last) {
			assert.ok(d.stats.last >= Date.now() - 6 * 30 * 24 * 60 * 60 * 1000);
		}
		else {
			assert.equal(d.stats.amount, 0);
		}
		
		nbCellsReady++;
	});
	
	dm1.askStatistics(bbox, null, true)
	.then(stats => {
		setTimeout(() => {
// 			console.log(stats);
			assert.equal(Object.keys(stats).length, nbCellsReady);
			
			for(let s in stats) {
				let stat = stats[s];
				assert.ok(/^-?[0-9]+(\.[0-9]+)?(,-?[0-9]+(\.[0-9]+)?){3}$/.test(s));
				assert.ok(!isNaN(stat.amount));
				assert.ok(stat.approxAmount !== undefined);
				if(stat.last) {
					assert.ok(stat.last >= Date.now() - 6 * 30 * 24 * 60 * 60 * 1000);
				}
				else {
					assert.equal(stat.amount, 0);
				}
			}
			
			done();
		}, 1000);
	})
	.catch(e => {
		assert.fail(e);
		done();
	});
});

QUnit.test("askStatistics fresh over not empty area with time range defined", function(assert) {
	let bbox = L.latLngBounds(L.latLng(48.1040,-1.6915), L.latLng(48.1182,-1.6658));
	let dm1 = new DataManager();
	let done = assert.async();
	let nbCellsReady = 0;
	
	dm1.on("statscellready", (d) => {
		assert.ok(/^-?[0-9]+(\.[0-9]+)?(,-?[0-9]+(\.[0-9]+)?){3}$/.test(d.bbox));
		assert.ok(!isNaN(d.stats.amount));
		assert.ok(d.stats.approxAmount !== undefined);
		if(d.stats.last) {
			assert.ok(d.stats.last >= 1464739200000);
			assert.ok(d.stats.last <= 1483228800000);
		}
		else {
			assert.equal(d.stats.amount, 0);
		}
		
		nbCellsReady++;
	});
	
	dm1.askStatistics(bbox, { start: 1464739200123, end: 1483228799000 }, true)
	.then(stats => {
		setTimeout(() => {
// 			console.log(stats);
			assert.ok(Object.keys(stats).length > 0);
			assert.equal(Object.keys(stats).length, nbCellsReady);
			
			for(let s in stats) {
				let stat = stats[s];
				assert.ok(/^-?[0-9]+(\.[0-9]+)?(,-?[0-9]+(\.[0-9]+)?){3}$/.test(s));
				assert.ok(!isNaN(stat.amount));
				assert.ok(stat.approxAmount !== undefined);
				if(stat.last) {
					assert.ok(stat.last >= 1464739200000);
					assert.ok(stat.last <= 1483228800000);
				}
				else {
					assert.equal(stat.amount, 0);
				}
			}
			done();
		}, 1000);
	})
	.catch(e => {
		assert.fail(e);
		done();
	});
});

QUnit.test("askStatistics cached over not empty area", function(assert) {
	let bbox = L.latLngBounds(L.latLng(48.1040,-1.6915), L.latLng(48.1182,-1.6658));
	let dm1 = new DataManager();
	let done = assert.async();
	let nbCellsReady = 0;
	
	dm1.askStatistics(bbox, null, true)
	.then(stats1 => {
		assert.ok(Object.keys(stats1).length > 0);
		
		dm1.on("statscellready", (d) => {
			assert.ok(/^-?[0-9]+(\.[0-9]+)?(,-?[0-9]+(\.[0-9]+)?){3}$/.test(d.bbox));
			assert.ok(!isNaN(d.stats.amount));
			assert.ok(d.stats.approxAmount !== undefined);
			if(d.stats.last) {
				assert.ok(d.stats.last >= Date.now() - 6 * 30 * 24 * 60 * 60 * 1000);
			}
			else {
				assert.equal(d.stats.amount, 0);
			}
			
			nbCellsReady++;
		});
		
		dm1.askStatistics(bbox, null, false)
		.then(stats => {
			setTimeout(() => {
				assert.equal(Object.keys(stats).length, nbCellsReady);
			
				for(let s in stats) {
					let stat = stats[s];
					assert.ok(/^-?[0-9]+(\.[0-9]+)?(,-?[0-9]+(\.[0-9]+)?){3}$/.test(s));
					assert.ok(!isNaN(stat.amount));
					assert.ok(stat.approxAmount !== undefined);
					if(stat.last) {
						assert.ok(stat.last >= Date.now() - 6 * 30 * 24 * 60 * 60 * 1000);
					}
					else {
						assert.equal(stat.amount, 0);
					}
				}
				
				done();
			}, 1000);
		})
		.catch(e => {
			assert.fail(e);
			done();
		});
	})
	.catch(e => {
		assert.fail(e);
		done();
	});
});

QUnit.test("askStatistics cached over not empty area with time range defined", function(assert) {
	let bbox = L.latLngBounds(L.latLng(48.1040,-1.6915), L.latLng(48.1182,-1.6658));
	let dm1 = new DataManager();
	let done = assert.async();
	let nbCellsReady = 0;
	
	dm1.askStatistics(bbox, { start: 1464739200123, end: 1483228799000 }, true)
	.then(stats1 => {
		assert.ok(Object.keys(stats1).length > 0);
		
		dm1.on("statscellready", (d) => {
			assert.ok(/^-?[0-9]+(\.[0-9]+)?(,-?[0-9]+(\.[0-9]+)?){3}$/.test(d.bbox));
			assert.ok(!isNaN(d.stats.amount));
			assert.ok(d.stats.approxAmount !== undefined);
			if(d.stats.last) {
				assert.ok(d.stats.last >= 1464739200000);
				assert.ok(d.stats.last <= 1483228800000);
			}
			else {
				assert.equal(d.stats.amount, 0);
			}
			
			nbCellsReady++;
		});
		
		dm1.askStatistics(bbox, { start: 1464739200123, end: 1483228799000 }, false)
		.then(stats => {
			setTimeout(() => {
				assert.equal(Object.keys(stats).length, nbCellsReady);
			
				for(let s in stats) {
					let stat = stats[s];
					assert.ok(/^-?[0-9]+(\.[0-9]+)?(,-?[0-9]+(\.[0-9]+)?){3}$/.test(s));
					assert.ok(!isNaN(stat.amount));
					assert.ok(stat.approxAmount !== undefined);
					if(stat.last) {
						assert.ok(stat.last >= 1464739200000);
						assert.ok(stat.last <= 1483228800000);
					}
					else {
						assert.equal(stat.amount, 0);
					}
				}
			
				done();
			}, 1000);
		})
		.catch(e => {
			assert.fail(e);
			done();
		});
	})
	.catch(e => {
		assert.fail(e);
		done();
	});
});

QUnit.test("askStatistics fresh over empty area", function(assert) {
	let bbox = L.latLngBounds(L.latLng(35.75, -44.78), L.latLng(35.76, -44.77));
	let dm1 = new DataManager();
	let done = assert.async();
	let nbCellsReady = 0;
	
	dm1.on("statscellready", (d) => {
		assert.ok(/^-?[0-9]+(\.[0-9]+)?(,-?[0-9]+(\.[0-9]+)?){3}$/.test(d.bbox));
		assert.ok(!isNaN(d.stats.amount));
		assert.ok(d.stats.approxAmount !== undefined);
		if(d.stats.last) {
			assert.ok(d.stats.last >= Date.now() - 6 * 30 * 24 * 60 * 60 * 1000);
		}
		else {
			assert.equal(d.stats.amount, 0);
		}
		
		nbCellsReady++;
	});
	
	dm1.askStatistics(bbox, null, true)
	.then(stats => {
		setTimeout(() => {
			assert.equal(Object.keys(stats).length, nbCellsReady);
			
			for(let s in stats) {
				let stat = stats[s];
				assert.ok(/^-?[0-9]+(\.[0-9]+)?(,-?[0-9]+(\.[0-9]+)?){3}$/.test(s));
				assert.ok(!isNaN(stat.amount));
				assert.equal(stat.amount, 0);
				assert.notOk(stat.approxAmount);
			}
		
			done();
		}, 1000);
	})
	.catch(e => {
		assert.fail(e);
		done();
	});
});


//roundDate
QUnit.test("roundDate", function(assert) {
	let dm1 = new DataManager();
	assert.equal(dm1.roundDate(1488993117000), 1488931200000);
});
