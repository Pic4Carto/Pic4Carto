/*
 * Test script for ctrl/service/URL.js
 */

let QUnit = require("qunitjs");
let Cell = require("../../../../src/js/model/Cell");
let L = require("leaflet");
let URL = require("../../../../src/js/ctrl/service/URL");

QUnit.module("Ctrl > Service > URL");

/*
 * CONSTRUCTOR
 */
QUnit.test("Constructor with valid parameters", function(assert) {
	let geturl = function() { return "http://pic4carto.net/?bbox=42.12,12.84,-7.23,-12.25"; };
	let seturl = function(url) { console.log("URL set to: "+url); };
	let u = new URL(geturl, seturl);
	
	assert.ok(u instanceof URL);
});


/*
 * ACCESSORS
 */

//getParameter
QUnit.test("getParameter with valid value", function(assert) {
	let geturl = function() { return "http://pic4carto.net/?bbox=42.12,12.84,-7.23,-12.25"; };
	let seturl = function(url) { console.log("URL set to: "+url); };
	let u = new URL(geturl, seturl);
	
	assert.equal(u.getParameter("bbox"), "42.12,12.84,-7.23,-12.25");
});
QUnit.test("getParameter with invalid value", function(assert) {
	let geturl = function() { return "http://pic4carto.net/?bbox=42.12,12.84,-7.23,-12.25"; };
	let seturl = function(url) { console.log("URL set to: "+url); };
	let u = new URL(geturl, seturl);
	
	assert.throws(
		function() { u.getParameter("missing") },
		new Error("ctrl.service.URL.invalidParameter")
	);
});
QUnit.test("getParameter with no query part from readURL", function(assert) {
	let geturl = function() { return "http://pic4carto.net/"; };
	let seturl = function(url) { console.log("URL set to: "+url); };
	let u = new URL(geturl, seturl);
	
	assert.equal(u.getParameter("bbox"), null);
});
QUnit.test("getParameter with a hash part and no query part from readURL", function(assert) {
	let geturl = function() { return "http://pic4carto.net/test.html#hashpart"; };
	let seturl = function(url) { console.log("URL set to: "+url); };
	let u = new URL(geturl, seturl);
	
	assert.equal(u.getParameter("bbox"), null);
});

//getQueryPart
QUnit.test("getQueryPart with valid value", function(assert) {
	let geturl = function() { return "http://pic4carto.net/?bbox=42.12,12.84,-7.23,-12.25"; };
	let seturl = function(url) { console.log("URL set to: "+url); };
	let u = new URL(geturl, seturl);
	
	assert.equal(u.getQueryPart(), "bbox=42.12,12.84,-7.23,-12.25");
});

QUnit.test("getQueryPart with a hash part and no query part from readURL", function(assert) {
	let geturl = function() { return "http://pic4carto.net/test.html#hashpart"; };
	let seturl = function(url) { console.log("URL set to: "+url); };
	let u = new URL(geturl, seturl);
	
	assert.equal(u.getQueryPart(), "");
});

//getCellOptions
QUnit.test("getCellOptions with all options defined", function(assert) {
	const geturl = function() { return "http://pic4carto.net/?bbox=42.12,12.84,-7.23,-12.25&from=1234&to=4567&fetchers=mapillary,flickr"; };
	const seturl = function(url) { console.log("URL set to: "+url); };
	const u = new URL(geturl, seturl);
	
	const result = u.getCellOptions();
	
	assert.equal(result.start, 1234);
	assert.equal(result.end, 4567);
	assert.equal(result.fetchers.length, 2);
	assert.equal(result.fetchers[0], "mapillary");
	assert.equal(result.fetchers[1], "flickr");
});

QUnit.test("getCellOptions with no options defined", function(assert) {
	const geturl = function() { return "http://pic4carto.net/?bbox=42.12,12.84,-7.23,-12.25"; };
	const seturl = function(url) { console.log("URL set to: "+url); };
	const u = new URL(geturl, seturl);
	
	const result = u.getCellOptions();
	
	assert.equal(result.start, null);
	assert.equal(result.end, null);
	assert.equal(result.fetchers, null);
});

//cellOptionsToQueryPart
QUnit.test("cellOptionsToQueryPart with fully defined cell", function(assert) {
	const c = new Cell(L.latLngBounds(L.latLng(0,1),L.latLng(2,3)), { start: 1234, end: 4567, fetchers: [ "mapillary", "flickr" ] });
	assert.equal(URL.cellOptionsToQueryPart(c), "from=1234&to=4567&fetchers=mapillary,flickr");
});

QUnit.test("cellOptionsToQueryPart with no options in cell", function(assert) {
	const c = new Cell(L.latLngBounds(L.latLng(0,1),L.latLng(2,3)));
	assert.equal(URL.cellOptionsToQueryPart(c), "");
});


/*
 * MODIFIERS
 */

//setParameter
QUnit.test("setParameter with valid value", function(assert) {
	let done = assert.async();
	let geturl = function() { return "http://pic4carto.net/?bbox=42.12,12.84,-7.23,-12.25#hashpart"; };
	let seturl = function(url) { assert.equal(url, "http://pic4carto.net/?bbox=1,2,3,4#hashpart"); done(); };
	let u = new URL(geturl, seturl);
	
	u.setParameter("bbox", "1,2,3,4");
	u.write();
});
QUnit.test("setParameter with invalid value", function(assert) {
	let geturl = function() { return "http://pic4carto.net/?bbox=42.12,12.84,-7.23,-12.25#hashpart"; };
	let seturl = function(url) { console.log("URL set to: "+url); };
	let u = new URL(geturl, seturl);
	
	assert.throws(
		function() { u.setParameter("missing", "nope") },
		new Error("ctrl.service.URL.invalidParameter")
	);
});
